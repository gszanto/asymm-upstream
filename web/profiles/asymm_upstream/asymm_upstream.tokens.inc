<?php

/**
 * @file
 * Set up custom tokens for Asymm Upstream.
 * 
 * Developed by Gabor Szanto.
 *  hello@szantogabor.com
 *  http://szantogabor.com
 */

use Drupal\image\Entity\ImageStyle;

/**
 * Implements hook_token_info().
 */
function asymm_upstream_token_info() {
  return [
    'types' => [
      'asymm-upstream-site' => [
        'name' => t('Asymm Upstream Site'),
        'description' => t('Custom site wide tokens.'),
      ],
      'asymm-upstream-node' => [
        'name' => t('Asymm Upstream Node'),
        'description' => t('Custom tokens for nodes.'),
      ],
      'asymm-upstream-media' => [
        'name' => t('Asymm Upstream Media'),
        'description' => t('Custom tokens for media..'),
        'needs-data' => 'media',
      ],
    ],
    'tokens' => [
      'asymm-upstream-site' => [
        'og-image' => [
          'name' => t("OG Image"),
          'description' => t('Prepare OG image urls.'),
        ],
      ],
      'asymm-upstream-media' => [
        'image-title' => [
          'name' => t("Image title"),
          'description' => t('Not working yet.'),
        ],
        'video-title' => [
          'name' => t("Video title"),
          'description' => t('Not working yet.'),
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function asymm_upstream_tokens($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  /** @var Drupal\node\Entity\Node $node */
  $node = !empty($data['node']) ? $data['node'] : FALSE;
  /** @var Drupal\media\Entity\Media $media */
  $media = !empty($data['media']) ? $data['media'] : FALSE;

  if ($type == 'asymm-upstream-node' && $node) {
    foreach ($tokens as $name => $original) {
      $bundle = $node->bundle();
    }
  }

  if ($type == 'asymm-upstream-media' && $media) {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'image-title':
          $replacements[$original] = 'todo';
          break;
        case 'video-title':
          $replacements[$original] = 'todo';
          break;
      }
    }
  }

  foreach ($tokens as $name => $original) {
    $entity = _asymm_upstream_get_route_entity();
    $entity_type = $entity ? $entity->getEntityTypeId() : NULL;
    $bundle = $entity ? $entity->bundle() : NULL;

    switch ($name) {
      case 'og-image':
        $type = ImageStyle::load('col_6');
        $image = FALSE;
        if (!empty($entity->field_featured_image)) {
          $image = $entity->field_featured_image->entity->image->entity->uri->value;;
        }

        if ($image) {
          $replacements[$original] = $type->buildUrl($image);;
        }
        break;
    }
  }

  return $replacements;
}
