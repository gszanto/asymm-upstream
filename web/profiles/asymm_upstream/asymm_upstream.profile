<?php

/**
 * @file
 * Developed by Gabor Szanto.
 *  hello@szantogabor.com
 *  http://szantogabor.com
 *
 * Enable modules and site configuration for a asymm_upstream site installation.
 */

// An ugly workaround, because PhpStorm doesn't have Drupal support on .profile
// files.

include_once (__DIR__ . "/asymm_upstream.module");
