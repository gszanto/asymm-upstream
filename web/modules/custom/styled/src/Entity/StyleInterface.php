<?php

namespace Drupal\styled\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Style entities.
 *
 * @ingroup styled
 */
interface StyleInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Style name.
   *
   * @return string
   *   Name of the Style.
   */
  public function getName();

  /**
   * Sets the Style name.
   *
   * @param string $name
   *   The Style name.
   *
   * @return \Drupal\styled\Entity\StyleInterface
   *   The called Style entity.
   */
  public function setName($name);

  /**
   * Gets the Style creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Style.
   */
  public function getCreatedTime();

  /**
   * Sets the Style creation timestamp.
   *
   * @param int $timestamp
   *   The Style creation timestamp.
   *
   * @return \Drupal\styled\Entity\StyleInterface
   *   The called Style entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Style published status indicator.
   *
   * Unpublished Style are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Style is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Style.
   *
   * @param bool $published
   *   TRUE to set this Style to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\styled\Entity\StyleInterface
   *   The called Style entity.
   */
  public function setPublished($published);

  /**
   * Gets the Style revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Style revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\styled\Entity\StyleInterface
   *   The called Style entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Style revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Style revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\styled\Entity\StyleInterface
   *   The called Style entity.
   */
  public function setRevisionUserId($uid);

}
