<?php

namespace Drupal\styled;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\styled\Entity\StyleInterface;

/**
 * Defines the storage handler class for Style entities.
 *
 * This extends the base storage class, adding required special handling for
 * Style entities.
 *
 * @ingroup styled
 */
class StyleStorage extends SqlContentEntityStorage implements StyleStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(StyleInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {style_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {style_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(StyleInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {style_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('style_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
