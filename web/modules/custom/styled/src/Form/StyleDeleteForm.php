<?php

namespace Drupal\styled\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Style entities.
 *
 * @ingroup styled
 */
class StyleDeleteForm extends ContentEntityDeleteForm {


}
