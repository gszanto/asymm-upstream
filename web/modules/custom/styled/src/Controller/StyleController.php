<?php

namespace Drupal\styled\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\styled\Entity\StyleInterface;

/**
 * Class StyleController.
 *
 *  Returns responses for Style routes.
 */
class StyleController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Style  revision.
   *
   * @param int $style_revision
   *   The Style  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($style_revision) {
    $style = $this->entityManager()->getStorage('style')->loadRevision($style_revision);
    $view_builder = $this->entityManager()->getViewBuilder('style');

    return $view_builder->view($style);
  }

  /**
   * Page title callback for a Style  revision.
   *
   * @param int $style_revision
   *   The Style  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($style_revision) {
    $style = $this->entityManager()->getStorage('style')->loadRevision($style_revision);
    return $this->t('Revision of %title from %date', ['%title' => $style->label(), '%date' => format_date($style->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Style .
   *
   * @param \Drupal\styled\Entity\StyleInterface $style
   *   A Style  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(StyleInterface $style) {
    $account = $this->currentUser();
    $langcode = $style->language()->getId();
    $langname = $style->language()->getName();
    $languages = $style->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $style_storage = $this->entityManager()->getStorage('style');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $style->label()]) : $this->t('Revisions for %title', ['%title' => $style->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all style revisions") || $account->hasPermission('administer style entities')));
    $delete_permission = (($account->hasPermission("delete all style revisions") || $account->hasPermission('administer style entities')));

    $rows = [];

    $vids = $style_storage->revisionIds($style);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\styled\StyleInterface $revision */
      $revision = $style_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $style->getRevisionId()) {
          $link = $this->l($date, new Url('entity.style.revision', ['style' => $style->id(), 'style_revision' => $vid]));
        }
        else {
          $link = $style->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => Url::fromRoute('entity.style.revision_revert', ['style' => $style->id(), 'style_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.style.revision_delete', ['style' => $style->id(), 'style_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['style_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
