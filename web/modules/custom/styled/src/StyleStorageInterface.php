<?php

namespace Drupal\styled;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\styled\Entity\StyleInterface;

/**
 * Defines the storage handler class for Style entities.
 *
 * This extends the base storage class, adding required special handling for
 * Style entities.
 *
 * @ingroup styled
 */
interface StyleStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Style revision IDs for a specific Style.
   *
   * @param \Drupal\styled\Entity\StyleInterface $entity
   *   The Style entity.
   *
   * @return int[]
   *   Style revision IDs (in ascending order).
   */
  public function revisionIds(StyleInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Style author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Style revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\styled\Entity\StyleInterface $entity
   *   The Style entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(StyleInterface $entity);

  /**
   * Unsets the language for all Style with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
