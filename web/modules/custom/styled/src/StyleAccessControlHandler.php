<?php

namespace Drupal\styled;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Style entity.
 *
 * @see \Drupal\styled\Entity\Style.
 */
class StyleAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\styled\Entity\StyleInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished style entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published style entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit style entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete style entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add style entities');
  }

}
