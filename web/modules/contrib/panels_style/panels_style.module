<?php

/**
 * @file
 * Contains panels_style.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
// use Drupal\panels_style\Plugin\PanelsStyle\PanelsStyleInterface;
use Drupal\panels\Form\PanelsContentForm;
use Drupal\panels\Plugin\DisplayVariant\PanelsDisplayVariant;

/**
 * Implements hook_help().
 */
function panels_style_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the panels_style module.
    case 'help.page.panels_style':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Adds a Panels Style plugin type to support creating configurable style plugins for panels blocks and regions.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_panels_build_alter()
 */
function panels_style_panels_build_alter(array &$build, PanelsDisplayVariant $panels_display)
{
  $display_config = $panels_display->getConfiguration();
  $style_helper = \Drupal::service('panels_style.helper');
  // Add support for UI patterns being used as layouts
  // Note that there may be other modules like UI Patterns that adapt their own
  // render element or theme hook implementations to be used by panels. In those
  // cases this alter could fail if the regions aren't stored as children of the
  // $build array. If we could alter panels region output any other, it may make
  // more sense to alter each region and block individually, but this won't be
  // possible until something like this task lands:
  // @see https://www.drupal.org/project/panels/issues/2820617
  if (isset($build['#type']) && $build['#type'] == 'pattern') {
    $keys = array_keys($build['#fields']);
    $regions = &$build['#fields'];
  }
  else {
    // Otherwise we assume it's just a normal render element with the region
    // render arrays as children
    $keys = Element::children($build);
    $regions = &$build;
  }
  foreach ($keys as $region_id) {
    // // Process styles for blocks in region
    foreach (Element::children($regions[$region_id]) as $block_id) {
      if (!empty($display_config['blocks'][$block_id]['style'])) {
        $style_plugin = $style_helper->getBlockStyle($display_config['blocks'][$block_id]['style']);
        $style_plugin->processBlock($regions[$region_id][$block_id], $block_id, $panels_display);
      }
    }

    // Process region after blocks have been processed in case region has
    // special cases for block style processsing.
    if (!empty($display_config['region_styles'][$region_id])) {
      $style_plugin = $style_helper->getRegionStyle($display_config['region_styles'][$region_id]);
      $style_plugin->processRegion($regions[$region_id], $region_id, $panels_display);
    }
  }
}

/***********************************************************************
 *
 * PANELS FORM ALTERING (STANDARD BUILDER)
 *
 ***********************************************************************/

/**
 * Implements hook_form_FORM_ID_alter().
 */
function panels_style_form_panels_add_block_form_alter(&$form, &$form_state) {
  // Just call the edit form alter since we're doing the same for either form.
  panels_style_form_panels_edit_block_form_alter($form, $form_state);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function panels_style_form_panels_edit_block_form_alter(&$form, FormStateInterface &$form_state) {

  // Load the display_variant and the block plugin
  $machine_name = $form_state->get('machine_name');
  $block_id = $form_state->get('block_id');
  $tempstore_id = $form_state->getBuildInfo()['args'][0];
  $panels_display = \Drupal::service('panels_style.helper')->getTempstoreDisplayVariant($machine_name, $tempstore_id);
  $display_config = $panels_display->getConfiguration();
  // If we have config at this point, we're loading an existing block
  if ($form_state->getValue('style')) {
    $block_style_config = ['plugin_id' => $form_state->getValue('style')];
  }
  else {
    $block_style_config = !empty($display_config['blocks'][$block_id]['style']) ? $display_config['blocks'][$block_id]['style'] : [];
  }

  // Load the style plugin for this block
  $style_plugin = \Drupal::service('panels_style.helper')->getBlockStyle($block_style_config);

  // Set it on form state storage for our validate/submit handlers. Namespace
  // it so it's clear where this is coming from.
  $form_state->set('panels_style_plugin', $style_plugin);

  // Create the plugin selector
  $plugin_options = \Drupal::service('plugin.manager.panels_style.style')->getPluginOptions('block');
  $form['style'] = [
    '#type'          => 'select',
    '#title'         => t('Style Plugin'),
    '#description'   => t('Please select the style plugin for this block.'),
    '#options'       => $plugin_options,
    '#default_value' => $style_plugin->getPluginId(),
    '#ajax' => array(
      'callback' => 'panels_style_form_panels_edit_block_form_select_style_ajax',
      'wrapper' => 'edit-style-settings',
      'progress' => array(
        'type' => 'throbber',
        'message' => t('Updating style settings...'),
      ),
    ),
  ];

  // Build the plugin configuration form into the block form
  $form['style_settings'] = $style_plugin->buildConfigurationForm([], (new FormState())->setValues($form_state->getValue('style_settings', [])));
  $form['style_settings']['#prefix'] = '<div id="edit-style-settings">';
  $form['style_settings']['#suffix'] = '</div>';
  // We want these values to be keyed with the parents in form_state.
  $form['style_settings']['#tree'] = TRUE;

  // Make sure the actions are at the bottom
  $form['actions']['#weight'] = 10;

  // Add our validate & submit handlers
  $form['#validate'][] = 'panels_style_form_panels_edit_block_form_validate';
  $form['#submit'][] = 'panels_style_form_panels_edit_block_form_submit';
}

/**
 * AJAX Callback when a style plugin is select from the drop down.
 */
function panels_style_form_panels_edit_block_form_select_style_ajax(array &$form, FormStateInterface $form_state) {
  return $form['style_settings'];
}

/**
 * Validate handler for the panels edit block form.
 */
function panels_style_form_panels_edit_block_form_validate(&$form, FormStateInterface &$form_state) {

  // Run the selected style plugin's validate handler
  $style_plugin = $form_state->get('panels_style_plugin');
  $style_settings = (new FormState())->setValues($form_state->getValue('style_settings', []));
  $style_plugin->validateConfigurationForm($form['style_settings'], $style_settings);
  if ($errors = $style_settings->getErrors()) {
    foreach ($errors as $name => $error) {
      $form_state->setErrorByName($name, $error);
    }
  }
  // Set the style settings value after validation in case the plugin altered
  // some values
  $form_state->setValue('style_settings', $style_settings->getValues());

}

/**
 * Submit handler for the panels edit block form.
 */
function panels_style_form_panels_edit_block_form_submit(array &$form, FormStateInterface $form_state) {

  // Pull data out of form_state
  $machine_name = $form_state->get('machine_name');
  $tempstore_id = $form_state->getBuildInfo()['args'][0];

  // Get the style settings
  $style_plugin = $form_state->get('panels_style_plugin');
  $style_settings = (new FormState())->setValues($form_state->getValue('style_settings', []));
  $style_plugin->submitConfigurationForm($form['style_settings'], $style_settings);

  // Is this necessary?
  // $form_state->setValue('style_settings', $style_plugin->getConfiguration());

  // Update the block config on the display variant
  \Drupal::service('panels_style.helper')->updateTempstoreDisplayVariantBlockConfig($form_state->get('block_id'), $style_plugin, $tempstore_id, $machine_name);
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Much of the code in this is adapted directly from
 * PanelsContentForm::buildForm().
 */
function panels_style_form_panels_block_page_content_alter(&$form, &$form_state) {

  // This block of code setting these vars is basically copied from the
  // PanelsContentForm form build method
  $tempstore_id = $form_state->getFormObject()->getTempstoreId();
  $cached_values = $form_state->getTemporaryValue('wizard');
  $variant_plugin = $cached_values['plugin'];
  $pattern_plugin = $variant_plugin->getPattern();
  $machine_name = $pattern_plugin->getMachineName($cached_values);

  // Set up the attributes used by a modal to prevent duplication later.
  $attributes = PanelsContentForm::getAjaxAttributes();

  if ($block_assignments = $variant_plugin->getRegionAssignments()) {
    // Loop through the blocks per region.
    foreach ($block_assignments as $region => $blocks) {
      $region_operations['edit'] = [
        'title' => t('Edit'),
        'url' => \Drupal::service('panels_style.helper')->getRegionEditUrl($tempstore_id, $machine_name, $region, \Drupal::request()->getRequestUri()),
        'attributes' => $attributes,
      ];

      // Make the region colspan one col shorter and add our edit operations
      $key = 'region-' . $region;
      $form['blocks'][$key]['title']['#wrapper_attributes']['colspan'] = 4;
      $form['blocks'][$key]['operations'] = [
        '#type' => 'operations',
        '#links' => $region_operations,
        '#wrapper_attributes' => [
          'colspan' => 1,
        ],
      ];
    }
  }
}

/***********************************************************************
 *
 * FORM ALTERING (IPE)
 *
 ***********************************************************************/



/**
 * Implements hook_form_FORM_ID_alter().
 */
function panels_style_form_panels_ipe_block_plugin_form_alter(&$form, &$form_state, $form_id) {

  // Load the display_variant and the block plugin
  $panels_display = $form_state->getBuildInfo()['args'][1];
  $block_id = $form_state->getBuildInfo()['args'][2];
  $display_config = $panels_display->getConfiguration();

  // If we have config at this point, we're loading an existing block
  if ($form_state->getValue(['settings', 'style'])) {
    $block_style_config = ['plugin_id' => $form_state->getValue(['settings', 'style'])];
  }
  else {
    $block_style_config = !empty($display_config['blocks'][$block_id]['style']) ? $display_config['blocks'][$block_id]['style'] : [];
  }

  // Load the style plugin for this block
  $style_plugin = \Drupal::service('panels_style.helper')->getBlockStyle($block_style_config);

  // Set it on form state storage for our validate/submit handlers. Namespace
  // it so it's clear where this is coming from.
  $form_state->set('panels_style_plugin', $style_plugin);

  // Create the plugin selector
  $plugin_options = \Drupal::service('plugin.manager.panels_style.style')->getPluginOptions('block');
  $form['flipper']['front']['settings']['style'] = [
    '#type'          => 'select',
    '#title'         => t('Style Plugin'),
    '#description'   => t('Please select the style plugin for this block.'),
    '#options'       => $plugin_options,
    '#default_value' => $style_plugin->getPluginId(),
    '#ajax' => array(
      'callback' => 'panels_style_form_panels_ipe_block_plugin_form_select_style_ajax',
      'wrapper' => 'edit-style-settings',
      'progress' => array(
        'type' => 'throbber',
        'message' => t('Updating style settings...'),
      ),
    ),
  ];

  // Build the plugin configuration form into the block form
  $form['flipper']['front']['settings']['style_settings'] = $style_plugin->buildConfigurationForm([], (new FormState())->setValues($form_state->getValue(['settings', 'style_settings'], [])));
  $form['flipper']['front']['settings']['style_settings']['#prefix'] = '<div id="edit-style-settings">';
  $form['flipper']['front']['settings']['style_settings']['#suffix'] = '</div>';
  // We want these values to be keyed with the parents in form_state.
  $form['flipper']['front']['settings']['style_settings']['#tree'] = TRUE;

  // Add our validate & submit handlers
  $form['#validate'][] = 'panels_style_form_panels_ipe_block_plugin_form_validate';
  $form['#submit'][] = 'panels_style_form_panels_ipe_block_plugin_form_submit';

  // Override the ajax submit handler too
  $form['submit']['#ajax']['callback'] = 'panels_style_form_panels_ipe_block_plugin_form_submit';
}

/**
 * AJAX Callback when a style plugin is select from the drop down.
 */
function panels_style_form_panels_ipe_block_plugin_form_select_style_ajax(array &$form, FormStateInterface $form_state) {
  return $form['flipper']['front']['settings']['style_settings'];
}

/**
 * Validate handler for the panels edit block form.
 */
function panels_style_form_panels_ipe_block_plugin_form_validate(&$form, FormStateInterface &$form_state) {

  // Run the selected style plugin's validate handler
  $style_plugin = $form_state->get('panels_style_plugin');
  $style_settings = (new FormState())->setValues($form_state->getValue(['settings', 'style_settings'], []));
  $style_plugin->validateConfigurationForm($form['flipper']['front']['settings']['style_settings'], $style_settings);
  if ($errors = $style_settings->getErrors()) {
    foreach ($errors as $name => $error) {
      $form_state->setErrorByName($name, $error);
    }
  }
  // Set the style settings value after validation in case the plugin altered
  // some values
  $form_state->setValue('style_settings', $style_settings->getValues());

}

/**
 * Submit handler for the panels edit block form.
 */
function panels_style_form_panels_ipe_block_plugin_form_submit(array &$form, FormStateInterface $form_state) {

  $form_object = $form_state->getBuildInfo()['callback_object'];
  $form = $form_object->submitForm($form, $form_state);

  // Get the style settings
  $style_plugin = $form_state->get('panels_style_plugin');
  $style_settings = (new FormState())->setValues($form_state->getValue(['settings', 'style_settings'], []));
  $style_plugin->submitConfigurationForm($form['flipper']['front']['settings']['style_settings'], $style_settings);

  $panels_display = $form_state->getBuildInfo()['args'][1];

  \Drupal::service('panels_style.helper')->updateIPETempstoreDisplayVariantBlockConfig($form_state->getValue('uuid'), $style_plugin, $panels_display);

  return $form;
}