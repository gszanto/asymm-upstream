<?php

namespace Drupal\panels_style\Plugin\PanelsStyle;

use Drupal\Core\Form\FormStateInterface;
use Drupal\panels\Plugin\DisplayVariant\PanelsDisplayVariant;

/**
 * Defines the default panels style plugin.
 *
 * @PanelsStyle(
 *   id = "panels_broken",
 *   title = @Translation("Missing/Broken")
 * )
 */
class PanelsStyleBroken extends PanelsStyleBase {

  /**
   * {@inheritdoc}
   */
  public function processRegion(array &$build, $region_id, PanelsDisplayVariant $panels_display) {
    \Drupal::logger('panels_style')->alert('The panels style plugin named "@style" is missing for region "@region" on the variant named "@variant" (id: "@id")', [
      '@style' => $panels_display->getConfiguration()['region_styles'][$region_id]['plugin'],
      '@region' => $region_id,
      '@variant' => $panels_display->label(),
      '@id' => $panels_display->getStorageId(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function processBlock(array &$build, $block_id, PanelsDisplayVariant $panels_display) {
    \Drupal::logger('panels_style')->alert('The panels style plugin named "@style" is missing for block "@block" on the variant named "@variant" (id: "@id")', [
      '@block' => $build['#configuration']['label'],
      '@style' => $build['#configuration']['style']['plugin'],
      '@variant' => $panels_display->label(),
      '@id' => $panels_display->getStorageId(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['broken_message'] = [
      '#markup' => $this->t('<em>The panels style plugin is missing for this block or region.</em>'),
    ];
    return $form;
  }

}
