<?php

namespace Drupal\panels_style\Plugin\PanelsStyle;

use Drupal\panels\Plugin\DisplayVariant\PanelsDisplayVariant;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;

/**
 * Defines the default panels style plugin.
 *
 * @PanelsStyle(
 *   id = "panels_default",
 *   title = @Translation("Default")
 * )
 */
class PanelsStyleDefault extends PanelsStyleBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'classes' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processRegion(array &$build, $region_id, PanelsDisplayVariant $panels_display) {
    $config = $this->getConfiguration();
    if ($config['classes']) {
      // Since the display builder is a plugin there's no way to be sure exactly
      // how the build will be, but at least for the standard builder and the
      // IPE we know it's just a #prefix
      if (isset($build['#prefix'])) {
        $region_name = Html::getClass("block-region-$region");
        $build['#prefix'] = '<div class="' . $region_name . ' ' . $config['classes'] . '">';
      }
      elseif (isset($build['#attributes'])) {
        // Maybe future-proof?
        foreach (explode(' ', $config['classes']) as $class) {
          $build['#attributes']['class'][] = $class;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processBlock(array &$build, $block_id, PanelsDisplayVariant $panels_display) {
    $config = $this->getConfiguration();
    if ($config['classes']) {
      // Since the display builder is a plugin there's no way to be sure exactly
      // how the build will be, but at least for the standard builder and the
      // IPE we know it's just a '#theme' => 'block' element so we go with that.
      foreach (explode(' ', $config['classes']) as $class) {
        $build['#attributes']['class'][] = $class;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $config = $this->getConfiguration();

    $form['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additional Classes'),
      '#description' => $this->t('Enter a space separated list of classes to apply to the wrapper.'),
      '#default_value' => $config['classes'],
    ];

    return $form;
  }

}
