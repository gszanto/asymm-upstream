<?php

namespace Drupal\panels_style;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Url;
use Drupal\user\SharedTempStoreFactory;
use Drupal\panels\CachedValuesGetterTrait;
use Drupal\panels_style\PanelsStyleHelperInterface;
use Drupal\panels_style\PanelsStylePluginManagerInterface;
use Drupal\panels_style\Plugin\PanelsStyle\PanelsStyleInterface;

/**
 * Helper service for panels styles.
 */
class PanelsStyleHelper implements PanelsStyleHelperInterface {

  use CachedValuesGetterTrait;
  use DependencySerializationTrait;

  /**
   * The style plugin manager.
   *
   * @var \Drupal\panels_style\Plugin\PanelsStyle\PanelsStylePluginManagerInterface;
   */
  protected $styleManager;

  /**
   * The configuration manager.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface;
   */
  protected $configFactory;

  /**
   * Tempstore factory.
   *
   * @var \Drupal\user\SharedTempStoreFactory
   */
  protected $tempstore;

  /**
   * Constructs a new PanelsStyleHelper.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config_factory handler.
   * @param \Drupal\panels_style\Plugin\PanelsStyle\PanelsStylePluginManagerInterface $style_manager
   *   The panels_style plugin manager
   */
  public function __construct(ConfigFactoryInterface $config_factory, PanelsStylePluginManagerInterface $style_manager, SharedTempStoreFactory $tempstore) {
    $this->configFactory = $config_factory;
    $this->styleManager = $style_manager;
    $this->tempstore = $tempstore;

  }

  /**
   * {@inheritDoc}
   */
  public function getBlockStyle($block_style_config) {
    $default = $this->configFactory->get('panels.settings')->get('default_style_block');

    $plugin_id = !empty($block_style_config['plugin_id']) ? $block_style_config['plugin_id'] : $default;
    $plugin_configuration = !empty($block_style_config['configuration']) ? $block_style_config['configuration'] : [];

    return $this->styleManager->createInstance($plugin_id, $plugin_configuration);
  }

  /**
   * {@inheritDoc}
   */
  public function getRegionStyle($region_style_config) {
    $default = $this->configFactory->get('panels.settings')->get('default_style_region');

    $plugin_id = !empty($region_style_config['plugin_id']) ? $region_style_config['plugin_id'] : $default;
    $plugin_configuration = !empty($region_style_config['configuration']) ? $region_style_config['configuration'] : [];

    return $this->styleManager->createInstance($plugin_id, $plugin_configuration);
  }

  /**
   * {@inheritDoc}
   */
  public function getTempstoreDisplayVariant($machine_name, $tempstore_id) {

    // Adapted from PanelsBlockConfigureFormBase::buildForm()
    $cached_values = $this->getCachedValues($this->tempstore, $tempstore_id, $machine_name);
    $display_variant = $cached_values['plugin'];
    $contexts = $display_variant->getPattern()->getDefaultContexts($this->tempstore, $tempstore_id, $machine_name);
    $display_variant->setContexts($contexts);

    return $display_variant;
  }

  /**
   * {@inheritDoc}
   */
  public function updateTempstoreDisplayVariantBlockConfig($block_id, $style_plugin, $tempstore_id, $machine_name) {

    // Load the block off the cached values which is where panels stores the
    // rest of the block config in PanelsBlockConfigureFormBase::submitForm()
    $cached_values = $this->getCachedValues($this->tempstore, $tempstore_id, $machine_name);
    $block = $cached_values['plugin']->getBlock($block_id);

    // Load the style config off our style plugin
    $style_configuration = $style_plugin->getConfiguration();

    // Merge our style config into the block config
    $block_style_config = $block->getConfiguration();
    $block_style_config['style']['plugin_id'] = $style_plugin->getPluginId();
    $block_style_config['style']['configuration'] = $style_configuration;

    // Update the block on the cached display variant plugin and save
    $cached_values['plugin']->updateBlock($block_style_config['uuid'], $block_style_config);
    // PageManager specific handling.
    if (isset($cached_values['page_variant'])) {
      $cached_values['page_variant']->getVariantPlugin()->setConfiguration($cached_values['plugin']->getConfiguration());
    }
    $this->tempstore->get($tempstore_id)->set($cached_values['id'], $cached_values);
  }

  /**
   * {@inheritDoc}
   */
  public function updateIPETempstoreDisplayVariantBlockConfig($block_id, $style_plugin, $panels_display) {

    // If a temporary configuration for this variant exists, use it.
    $temp_store_key = $panels_display->getTempStoreId();
    if ($display_config = $this->tempstore->get('panels_ipe')->get($temp_store_key)) {
      $panels_display->setConfiguration($display_config);
    }

    // If a UUID is provided, the Block should already exist.
    $block = $panels_display->getBlock($block_id);

    // Load the style config off our style plugin
    $style_configuration = $style_plugin->getConfiguration();

    // Merge our style config into the block config
    $block_style_config = $block->getConfiguration();
    $block_style_config['style']['plugin_id'] = $style_plugin->getPluginId();
    $block_style_config['style']['configuration'] = $style_configuration;

    // Update the block on the cached display variant plugin and save
    $panels_display->updateBlock($block_style_config['uuid'], $block_style_config);

    // Set the tempstore value.
    $this->tempstore->get('panels_ipe')->set($panels_display->getTempStoreId(), $panels_display->getConfiguration());
  }

  /**
   * {@inheritdoc}
   */
  public function getRegionEditUrl($tempstore_id, $machine_name, $region, $destination = NULL) {
    return Url::fromRoute('panels_style.region_edit_style', [
      'tempstore_id' => $tempstore_id,
      'machine_name' => $machine_name,
      'region' => $region,
      'destination' => $destination,
    ]);
  }
}
