<?php

namespace Drupal\panels_style;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Plugin\FallbackPluginManagerInterface;

/**
 * Provides an interface for the discovery and instantiation of layout plugins.
 */
interface PanelsStylePluginManagerInterface extends PluginManagerInterface, FallbackPluginManagerInterface {

  /**
   * Get the style plugin options.
   *
   * @param string $type
   *   The type of the panels component being styled. Options are 'block' or
   *   'region'. All style plugins will be returned if not provided.
   * @return array
   *   An array of definition titles, keyed by
   */
  public function getPluginOptions($type = null);

}
