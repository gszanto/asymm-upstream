<?php

namespace Drupal\panels_style\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a PanelsStyle annotation object.
 *
 * @todo: Document other annotation keys.
 *
 * @Annotation
 */
class PanelsStyle extends Plugin {

  /**
   * The human readable title.
   *
   * @var string
   */
  public $title = '';

  /**
   * The human readable description.
   *
   * @var string
   */
  public $description = '';

  /**
   * Whether or not this style plugin can be used to render blocks.
   *
   * @var string
   */
  public $block = TRUE;

  /**
   * Whether or not this style plugin can be used to render regions.
   *
   * @var string
   */
  public $region = TRUE;

}
