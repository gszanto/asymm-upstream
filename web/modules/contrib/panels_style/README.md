# Panels Style

This module adds a new Panels Style plugin type for styling panels blocks and 
regions. This functionality used to be a part of the D7 panels module but was 
lost in the upgrade.

A few default style plugins are included, 

## History

This module was adapated from a patch in the Panels issue queue
(https://www.drupal.org/project/panels/issues/2296437), but an IRC conversation
between the module maintainers suggested that they may want to look at a higher
level solution. So, to avoid continued work on a patch that may never get 
committed, we decided to go the contrib route. The main discussion on this 
decision starts here https://www.drupal.org/project/panels/issues/2296437#comment-12414958

## Installation

If you're installing the module _without_ having previously had the patch 
applied, just enable the module.

## Usage

Once installed, go to the Panels content edit form (e.g. the "Content" section 
of a page manager page using Panels). Regions will now have an "Edit" action 
which will allow you to select a style plugin for the region. Additionally, the 
styles selector will be injected into the block edit form to select block 
styles.

There is also a global configuration page at /admin/config/content/panels_style 
that allows you to set the default panels style plugin. Note that this default 
wiil apply to any regions/blocks that existed even before enabling this module.

The module comes with a couple default plugins, but the real power of panels 
styles will come from writing your own style plugins.

## Writing your own style plugins

If you are unfamiliar with the Drupal Plugin API, you can read up on it here:
https://www.drupal.org/docs/8/api/plugin-api

An individual plugin can style blocks, regions, or both. You can also define 
a configuration form to let users customize the output of the blocks or regions. 
A base class is provided that should provide most of the default functionality: 
Drupal\panels_style\Plugin\PanelsStyle\PanelsStyleBase.

See the Drupal\panels_style\Plugin\PanelsStyle\PanelsStyleDefault for an example
of a plugin with a basic configuration form that styles both regions and blocks.

## Migration from a previously patched Panels

**Although I've tested this successfully on a fairly sizeable site that was 
relying heavily on the original styles patch, you'll want to make sure you 
backup your site before migrating and test thoroughly after.**

If you had previously had the Panels module style patch applied you will need to 
revert that patch just before installing. Otherwise, this module will migrate 
any existing panels blocks & regions panels styles on install so you shouldn't 
have to update any config.

You will, however, need to refactor any existing style plugins that you had 
written for the patched version of panels. This is because we're using alter 
hooks to inject our styles so the blocks/regions are already built by the time
we're styling them. So instead of the ::buildBlock() and ::buildRegion()
methods from before, you'll be using ::processBlock() and ::processRegion()
to alter the block/region builds and add your styles.

See the Drupal\panels_style\Plugin\PanelsStyle\PanelsStyleDefault for an example
of a plugin with a basic configuration form that styles both regions and blocks.

**_Also, note that any region and block style config is removed when the module 
is uninstalled. So if you decide to revert back to using the patch after 
installing the module, you'll lose any existing styles._**

## IPE Support

Note that there is only partial support for the Panels IPE module. The IPE 
block form includes the style selector, but regions support has not been added.
Patches for this would be welcome!
